import React, { useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import './App.css';
import { addTodo, toggleComplete, deleteTodo } from "./todoSlice";

const Todo = () => {
  const [text, setText] = useState("");
  const todos = useSelector((state) => state.todos);
  const dispatch = useDispatch();

  const handleInputChange = (e) => {
    setText(e.target.value);
  };

  const handleAddTodo = () => {
    if (text) {
      dispatch(addTodo(text));
      setText("");
    }
  };

  const handleToggleComplete = (id) => {
    dispatch(toggleComplete(id));
  };

  const handleDeleteTodo = (id) => {
    dispatch(deleteTodo(id));
  };

  return (
    <>
    <div style={{marginLeft: '42%', marginTop: '50px'}}>
      <input style={{width: '25%', backgroundColor: '' }} type="text" value={text} onChange={handleInputChange} />{" "}
      <button style={{width: '', backgroundColor: 'maroon', color: 'whitesmoke'}} onClick={handleAddTodo}> Add Todo </button>{" "}
      <ul>
        {" "}
        {todos.map((todo) => (
          <li
            key={todo.id}
            style={{
              textDecoration: todo.completed ? "line-through" : "none",
            }}
          >
            {todo.text}{" "}
            <button style={{width: '', backgroundColor: 'maroon', color: 'whitesmoke'}} onClick={() => handleToggleComplete(todo.id)}>
              {" "}
              {todo.completed ? "Mark Incomplete" : "Mark Complete"}{" "}
            </button>{" "}
            <button style={{width: '', backgroundColor: 'maroon', color: 'whitesmoke'}} onClick={() => handleDeleteTodo(todo.id)}> Delete </button>{" "}
          </li>
        ))}{" "}
      </ul>{" "}
    </div>

    </>

  );
};

export default Todo;